import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HomeDashboardRaceDetailsComponent } from './home-dashboard-race-details.component';

describe('HomeDashboardRaceDetailsComponent', () => {
  let component: HomeDashboardRaceDetailsComponent;
  let fixture: ComponentFixture<HomeDashboardRaceDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeDashboardRaceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDashboardRaceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
