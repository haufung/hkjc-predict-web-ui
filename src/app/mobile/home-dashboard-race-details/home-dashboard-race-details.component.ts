import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { PredictService } from '../../shared/services/predict/predict.service';
import { filter } from 'rxjs/operators';
import { NbDialogService  } from '@nebular/theme';

@Component({
  selector: 'app-home-dashboard-race-details',
  templateUrl: './home-dashboard-race-details.component.html',
  styleUrls: ['./home-dashboard-race-details.component.scss'],
  providers: [PredictService]
})
export class HomeDashboardRaceDetailsComponent implements OnInit {
  raceId: any;
  loading: any;
  latestModelVersion: any;
  recordList: any = [];
  raceInfo: any;
  selectedHorseNumber: any;
  _: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private predictService: PredictService,
    private dialogService: NbDialogService,
    private router: Router) {
    this.raceId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  async getThisCurrentRecordsData(id, version) {
    return new Promise<void>((resolve, reject) => {
      this.predictService.searchCurrentRecordsByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
        if (res && res.code === 200) {
          res.data.forEach(element => {
            element.confidence = parseFloat(element.confidence).toFixed(3);
          });
        }
        this.recordList = res.data
        resolve()
      });
    });
  }

  async getThisCurrentRaceData(id, version) {
    return new Promise<void>((resolve, reject) => {
      this.predictService.searchCurrentRaceByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
        if (res && res.code === 200 && res.data.length > 0) {
          this.raceInfo = res.data[0]
        }
        resolve()
      });
    });
  }

  async ngOnInit() {
    this.loading = true;
    this._ = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe((event: NavigationEnd) => this.checkRaceIdChanged(event));
    this.latestModelVersion = JSON.parse(localStorage.getItem('latestModelVersion'))[0].version;
    await this.getThisCurrentRecordsData(this.raceId, this.latestModelVersion);
    await this.getThisCurrentRaceData(this.raceId, this.latestModelVersion);
    this.loading = false;
  }

  async checkRaceIdChanged(event) {
    this.loading = true;
    this.raceId = this.activatedRoute.snapshot.paramMap.get('id');
    await this.getThisCurrentRecordsData(this.raceId, this.latestModelVersion);
    await this.getThisCurrentRaceData(this.raceId, this.latestModelVersion);
    this.loading = false;
  }

  open(dialog: TemplateRef<any>, horseNum) {
    this.selectedHorseNumber = horseNum
    this.dialogService.open(dialog);
  }

  ngOnDestroy() {
    this._.unsubscribe();
  }

}
