import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../shared/services/dashboard/dashboard.service';
import { mapRaceDataUtil } from '../../utils/mapRaceDataUtil';

@Component({
  selector: 'app-model-past-predict',
  templateUrl: './model-past-predict.component.html',
  styleUrls: ['./model-past-predict.component.scss', '../../shared/styles/main.scss']
})
export class ModelPastPredictComponent implements OnInit {
  modelPastPredict: any;
  latestModelVersion: any;
  latestVersionThreshold: any;
  constructor(
    private dashboardService: DashboardService) { }

  async getModelPastPredict(version, threshold, numberOfMonths) {
    return new Promise<void>((resolve, reject) => {
      this.dashboardService.getPastPredictWithinMonthsWithThreshold({ version: version, threshold: threshold, numberOfMonths: numberOfMonths }).subscribe((res: any) => {
        if (res && res.code === 200 && res.data.length > 0) {
          res.data.forEach(item => {
            var [raceNum, dateLabel] = mapRaceDataUtil.mapRaceLabelFromRaceId(item)
            var [raceNum, dateLabel] = mapRaceDataUtil.mapShortRaceLabelFromRaceId(item)
            item.race_id = dateLabel + `第${raceNum}場`
            item.short_race_id = dateLabel + `(${raceNum})`
            item.isWin = item.place == '1' || item.place == '1 DH' ? true : false
            item.firstRunner = item.place == '2' || item.place == '2 DH' ? true : false
            item.secondRunner = item.place == '3' || item.place == '3 DH' ? true : false
            item.longPlace = `第${item.place}名`
            item.round = parseFloat(item.round).toFixed(3);
          });
          this.modelPastPredict = res.data
        }
        resolve()
      });
    });
  }

  async ngOnInit() {
    this.latestModelVersion = JSON.parse(localStorage.getItem('latestModelVersion'))[0].version;
    JSON.parse(localStorage.getItem('modelThreshold')).map(model => {
      if (model.version == this.latestModelVersion) {
        this.latestVersionThreshold = model.threshold
      }
    });
    await this.getModelPastPredict(this.latestModelVersion, "0.5", "12")
  }

}
