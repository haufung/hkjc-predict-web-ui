import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModelPastPredictComponent } from './model-past-predict.component';

describe('ModelPastPredictComponent', () => {
  let component: ModelPastPredictComponent;
  let fixture: ComponentFixture<ModelPastPredictComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelPastPredictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelPastPredictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
