import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { PredictService } from '../../shared/services/predict/predict.service';
import { DashboardService } from '../../shared/services/dashboard/dashboard.service';
import { DxChartComponent } from "devextreme-angular";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-records-details',
  templateUrl: './records-details.component.html',
  styleUrls: ['./records-details.component.scss'],
  providers: [PredictService]
})
export class RecordsDetailsComponent implements OnInit {
  @ViewChild("targetChart", { static: false }) dxChart: DxChartComponent
  @Input() thisRaceId: string;
  @Input() selectedHorseNumber: string;

  constructor(private predictService: PredictService,
    private dashboardService: DashboardService,
    private translate: TranslateService) { }
  latestVersionThreshold: any;
  currentRaceData: any = [];
  allEloOfCurrentRace: any = [];
  averageHorseElo: any;
  averageJockeyElo: any;
  averageTrainerElo: any;
  horseEloSelected: boolean;
  jockeyEloSelected: boolean;
  trainerEloSelected: boolean;
  redColor = "#ff7c7c";
  darkRedColor = "#9B4B4B";
  defaultBarColor = "#e7d19a";
  darkBarColor = "#413C2B";
  defaultEloBarColor = "#e7d19a";
  darkEloBarColor = "#413C2B";
  horseEloColor = "#ff7c7c";
  darkHorseEloColor = "#9B4B4B";
  jockeyEloColor = "#AAAAFA";
  darkJockeyEloColor = "#54547A";
  trainerEloColor = "#00BFFF";
  darkTrainerEloColor = "#046E94";
  loading = false;
  horseEloBarColor: any;
  jockeyEloBarColor: any;
  trainerEloBarColor: any;
  latestModelVersion: any;
  thresholdText: any;
  basicInfo: any;

  async ngOnInit() {
    this.loading = true
    await this.getCurrentRecordsBasicInfoByRaceIdAndHorseNum(this.thisRaceId, this.selectedHorseNumber)
    this.latestModelVersion = JSON.parse(localStorage.getItem('latestModelVersion'))[0].version;
    this.averageHorseElo = localStorage.getItem('averageHorseElo')
    this.averageJockeyElo = localStorage.getItem('averageJockeyElo')
    this.averageTrainerElo = localStorage.getItem('averageTrainerElo')
    JSON.parse(localStorage.getItem('modelThreshold')).map(model => {
      if (model.version == this.latestModelVersion) {
        this.latestVersionThreshold = model.threshold
      }
    });
    this.thresholdText = `建議門檻-${this.latestVersionThreshold}`
    await this.getThisCurrentRecordsData(this.thisRaceId, this.latestModelVersion)
    await this.getAllEloByCurrentRaceId(this.thisRaceId, this.latestModelVersion)
    this.loading = false
  }

  async getCurrentRecordsBasicInfoByRaceIdAndHorseNum(raceId, horseNum) {
    return new Promise<void>((resolve, reject) => {
      this.predictService.getCurrentRecordsBasicInfoByRaceIdAndHorseNum({ raceId: raceId, horseNum: horseNum }).subscribe((res: any) => {
        if (res && res.code === 200) {
          this.basicInfo = res.data
        }
        resolve()
      });
    });
  }

  async getThisCurrentRecordsData(id, version) {
    return new Promise<void>((resolve, reject) => {
      this.predictService.searchCurrentRecordsByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
        if (res && res.code === 200) {
          res.data.forEach(element => {
            element.confidence = parseFloat(element.confidence).toFixed(3);
            element.horseNameAndNum = `${element.horse_number}-${element.chinese_name}`
            element.confidence = parseFloat(element.confidence)
            element.latestVersionThreshold = parseFloat(this.latestVersionThreshold)
            this.currentRaceData.push(element)
          });
        }
        resolve()
      });
    });
  }

  async getAllEloByCurrentRaceId(raceId, version) {
    return new Promise<void>((resolve, reject) => {
      this.dashboardService.getAllEloByCurrentRaceId({ version: version, race_id: raceId }).subscribe((res: any) => {
        if (res && res.code === 200 && res.data.length > 0) {
          res.data.forEach(item => {
            this.translate.get(item.jockey_name).subscribe((res: string) => {
              item.jockeyNameAndNum = `${item.horse_number}-${res}`
            });
            this.translate.get(item.trainer_name).subscribe((res: string) => {
              item.trainerNameAndNum = `${item.horse_number}-${res}`
            });
            item.horseNameAndNum = `${item.horse_number}-${item.horse_name}`
          })
          this.allEloOfCurrentRace = res.data
        }
        resolve()
      });
    });
  }

  customizeEloPoint = (arg: any) => {
    if (arg.data.horse_number == this.selectedHorseNumber) {
      if (this.horseEloSelected && arg.seriesName == '馬匹' && arg.value > this.averageHorseElo) {
        return { color: this.horseEloColor, hoverStyle: { color: this.horseEloColor } };
      } else if (this.jockeyEloSelected && arg.seriesName == '騎師' && arg.value > this.averageJockeyElo) {
        return { color: this.jockeyEloColor, hoverStyle: { color: this.jockeyEloColor } };
      } else if (this.trainerEloSelected && arg.seriesName == '練馬師' && arg.value > this.averageTrainerElo) {
        return { color: this.trainerEloColor, hoverStyle: { color: this.trainerEloColor } };
      }
    } else {
      if (this.horseEloSelected && arg.seriesName == '馬匹' && arg.value > this.averageHorseElo) {
        return { color: this.darkHorseEloColor, hoverStyle: { color: this.darkHorseEloColor } };
      } else if (this.jockeyEloSelected && arg.seriesName == '騎師' && arg.value > this.averageJockeyElo) {
        return { color: this.darkJockeyEloColor, hoverStyle: { color: this.darkJockeyEloColor } };
      } else if (this.trainerEloSelected && arg.seriesName == '練馬師' && arg.value > this.averageTrainerElo) {
        return { color: this.darkTrainerEloColor, hoverStyle: { color: this.darkTrainerEloColor } };
      } else {
        return { color: this.darkBarColor, hoverStyle: { color: this.darkBarColor } };
      }
    }
  }

  changeEloTab(event) {
    this.resetEloSelect()
    this.resetEloBarColor()
    if (event.tabTitle == '馬匹') {
      this.horseEloSelected = true
      this.horseEloBarColor = this.defaultEloBarColor
    } else if (event.tabTitle == '騎師') {
      this.jockeyEloSelected = true
      this.jockeyEloBarColor = this.defaultEloBarColor
    } else if (event.tabTitle == '練馬師') {
      this.trainerEloSelected = true
      this.trainerEloBarColor = this.defaultEloBarColor
    }
    this.dxChart.instance.refresh();
  }

  resetEloSelect() {
    this.horseEloSelected = false
    this.jockeyEloSelected = false
    this.trainerEloSelected = false
  }

  resetEloBarColor() {
    this.horseEloBarColor = this.darkEloBarColor
    this.jockeyEloBarColor = this.darkEloBarColor
    this.trainerEloBarColor = this.darkEloBarColor
  }


  customizePoint = (arg: any) => {
    if (arg.data.horse_number == this.selectedHorseNumber) {
      if (arg.value > this.latestVersionThreshold) {
        return { color: this.redColor, hoverStyle: { color: this.redColor } };
      } else {
        return { color: this.defaultBarColor, hoverStyle: { color: this.defaultBarColor } };
      }
    } else {
      if (arg.value > this.latestVersionThreshold) {
        return { color: this.darkRedColor, hoverStyle: { color: this.darkRedColor } };
      } else {
        return { color: this.darkBarColor, hoverStyle: { color: this.darkBarColor } };
      }
    }
  }

  customizeText = (arg: any) => {
    return arg.valueText;
  }
}
