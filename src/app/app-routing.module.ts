import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PredictionResultComponent } from './model/prediction-result/prediction-result.component';
import { PerformanceDashboardComponent } from './model/performance-dashboard/performance-dashboard.component';
import { AccuracyDashboardComponent } from './model/accuracy-dashboard/accuracy-dashboard.component';
import { HomeDashboardComponent } from './model/home-dashboard/home-dashboard.component';
import { ModelPastPredictComponent } from './mobile/model-past-predict/model-past-predict.component';
import { HomeDashboardRaceDetailsComponent } from './mobile/home-dashboard-race-details/home-dashboard-race-details.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/start', pathMatch: 'full' },
  { path: 'start', component: HomeDashboardComponent },
  { path: 'performance', component: PerformanceDashboardComponent },
  { path: 'result', component: PredictionResultComponent },
  { path: 'accuracy', component: AccuracyDashboardComponent },
  { path: 'mobile/past', component: ModelPastPredictComponent },
  { path: 'mobile/race/:id', component: HomeDashboardRaceDetailsComponent },
  { path: '**', component: PageNotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
