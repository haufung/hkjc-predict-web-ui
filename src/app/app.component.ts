import { Component, OnInit, HostListener } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ConfigService } from './shared/services/config/config.service';
import { ScrapperService } from './shared/services/scrapper/scrapper.service';
import { DashboardService } from './shared/services/dashboard/dashboard.service';
import { PredictService } from './shared/services/predict/predict.service';
import { MENU_ITEMS } from '../app/shared/data-models/menu-items';
import { SMALL_SCREEN_MENU_ITEMS } from '../app/shared/data-models/menu-items';
import * as moment from 'moment';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { NbMenuService, NbSidebarService } from '@nebular/theme';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './shared/styles/main.scss'],
  providers: [MessageService, DashboardService, PredictService]
})
export class AppComponent implements OnInit {
  loading = false;
  menuItems: any;
  isSmallScreen = false;
  nextRaceDate: any;
  currentUrl: any;
  _: any;
  raceList: any = [];
  showNextRaceSubHeader: boolean = false;
  showRaceSelectSubHeader: boolean = false;
  toggle = true;
  constructor(
    private configService: ConfigService,
    private dashboardService: DashboardService,
    private scrapperService: ScrapperService,
    private predictService: PredictService,
    private router: Router,
    private translate: TranslateService,
    private menu: NbMenuService,
    private sidebarService: NbSidebarService) {
      menu.onItemClick().subscribe(() => {
        if (this.isSmallScreen) {
          sidebarService.collapse()
        }
      });
  }


  checkWindowSize() {
    if (window.innerWidth < 795) {
      this.isSmallScreen = true
      this.menuItems = SMALL_SCREEN_MENU_ITEMS;
    } else {
      this.isSmallScreen = false
      this.menuItems = MENU_ITEMS;
    }
  }
  @HostListener('window: resize', ['$event']) onResize(event: any) {
    this.checkWindowSize()
  }

  async getModelVersion() {
    return new Promise<void>((resolve, reject) => {
      this.configService.getModelVersion().subscribe((res: any) => {
        if (res && res.code === 200) {
          localStorage.setItem('modelVersion', JSON.stringify(res.data));
          resolve()
        }
      })
    });
  }

  async getModelThreshold() {
    return new Promise<void>((resolve, reject) => {
      this.configService.getModelThreshold().subscribe((res: any) => {
        if (res && res.code === 200) {
          localStorage.setItem('modelThreshold', JSON.stringify(res.data));
          resolve()
        }
      })
    });
  }

  async getLatestModelVersion() {
    return new Promise<void>((resolve, reject) => {
      this.configService.getLatestModelVersion().subscribe((res: any) => {
        if (res && res.code === 200) {
          localStorage.setItem('latestModelVersion', JSON.stringify(res.data));
          resolve()
        }
      })
    });
  }

  async getTranslateMapping() {
    return new Promise<void>((resolve, reject) => {
      this.configService.getTranslateMapping().subscribe((res: any) => {
        if (res && res.code === 200) {
          var hkTranslate = {}
          var enTranslate = {}
          Object.assign(hkTranslate, res.data.raceLocation.chinese, res.data.raceClass.chinese, res.data.raceDistance.chinese,
            res.data.raceGoing.chinese, res.data.raceCourse.chinese, res.data.horseSex.chinese, res.data.raceName.chinese,
            res.data.jockeyName.chinese, res.data.trainerName.chinese);
          Object.assign(enTranslate, res.data.raceLocation.english, res.data.raceClass.english, res.data.raceDistance.english,
            res.data.raceGoing.english, res.data.raceCourse.english, res.data.horseSex.english, res.data.raceName.english,
            res.data.jockeyName.english, res.data.trainerName.english);
          this.translate.setTranslation('HK', hkTranslate);
          this.translate.setTranslation('EN', enTranslate);
          resolve()
        }
      })
    });
  }

  async getAllEloAverage() {
    return new Promise<void>((resolve, reject) => {
      this.dashboardService.getAllEloAverage().subscribe((res: any) => {
        if (res && res.code === 200) {
          localStorage.setItem('averageHorseElo', res.data[0].avg_horse_elo);
          localStorage.setItem('averageJockeyElo', res.data[0].avg_jockey_elo);
          localStorage.setItem('averageTrainerElo', res.data[0].avg_trainer_elo);
          resolve()
        }
      })
    });
  }

  async getNextRaceDate() {
    return new Promise<void>((resolve, reject) => {
      this.scrapperService.getNextRaceDate().subscribe((res: any) => {
        if (res && res.code === 200 && res.data.length > 0) {
          var date = moment(res.data).format('YYYY年M月DD日')
          localStorage.setItem('nextRaceDate', date);
          this.nextRaceDate = date
        }
        resolve()
      });
    });
  }

  async getAllCurrentRaceIdDetails() {
    return new Promise<void>((resolve, reject) => {
      this.predictService.getAllCurrentRaceIdDetails().subscribe((res: any) => {
        if (res && res.code === 200) {
          sessionStorage.setItem('currentRaceIdDetails', JSON.stringify(res.data));
          this.raceList = res.data
        }
        resolve()
      });
    })
  }

  async ngOnInit() {
    this._ = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => this.modifyHeader(event));
    this.loading = true;
    this.checkWindowSize()
    await this.getTranslateMapping()
    await this.getModelVersion()
    await this.getModelThreshold()
    await this.getLatestModelVersion()
    await this.getAllEloAverage()
    await this.getNextRaceDate()
    await this.getAllCurrentRaceIdDetails()
    this.loading = false
  }

  modifyHeader(location) {
    this.currentUrl = location.urlAfterRedirects
    if (location.urlAfterRedirects == "/start" && !this.showNextRaceSubHeader) {
      this.showNextRaceSubHeader = true;
      this.showRaceSelectSubHeader = false;
    } else if (location.urlAfterRedirects.includes("/mobile/race/") && !this.showRaceSelectSubHeader) {
      this.showNextRaceSubHeader = false;
      this.showRaceSelectSubHeader = true;
    }
  }

  onClickRaceNum(raceId) {
    this.router.navigate(['mobile/race/' + raceId]);
  }

  ngOnDestroy() {
    this._.unsubscribe();
  }
  title = 'mega-intelligence';
}
