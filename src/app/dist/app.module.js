"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var page_not_found_component_1 = require("./page-not-found/page-not-found.component");
var router_1 = require("@angular/router");
var crisis_list_component_1 = require("./crisis-list/crisis-list.component");
var table_1 = require("primeng/table");
var toast_1 = require("primeng/toast");
var button_1 = require("primeng/button");
var http_1 = require("@angular/common/http");
var theme_1 = require("@nebular/theme");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                crisis_list_component_1.CrisisListComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                platform_browser_1.BrowserModule,
                table_1.TableModule,
                button_1.ButtonModule,
                toast_1.ToastModule,
                http_1.HttpClientModule,
                theme_1.NbThemeModule.forRoot(),
                theme_1.NbLayoutModule,
                theme_1.NbSidebarModule.forRoot(),
                theme_1.NbButtonModule,
                theme_1.NbSelectModule,
                theme_1.NbCardModule,
                router_1.RouterModule.forRoot([
                    { path: '', redirectTo: '/start', pathMatch: 'full' },
                    { path: 'start', component: crisis_list_component_1.CrisisListComponent },
                    { path: '**', component: page_not_found_component_1.PageNotFoundComponent }
                ]),
                theme_1.NbSelectModule,
                theme_1.NbCardModule,
            ],
            providers: [],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
