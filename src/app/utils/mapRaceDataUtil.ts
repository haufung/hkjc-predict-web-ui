import * as moment from 'moment';

export class mapRaceDataUtil {
  static mapRaceLabelFromRaceId(item) {
    var idAndRaceNum = item.race_id.split("_")
    var raceNum = idAndRaceNum.pop()
    var dateId = idAndRaceNum.pop()
    var date = moment(dateId, 'YYYYMMDD')
    var dateLabel = date.format('YYYY年M月DD日');
    return [raceNum, dateLabel]
  }

  static mapShortRaceLabelFromRaceId(item) {
    var idAndRaceNum = item.race_id.split("_")
    var raceNum = idAndRaceNum.pop()
    var dateId = idAndRaceNum.pop()
    var date = moment(dateId, 'YYYYMMDD')
    var dateLabel = date.format('DD/M/YYYY');
    return [raceNum, dateLabel]
  }
}