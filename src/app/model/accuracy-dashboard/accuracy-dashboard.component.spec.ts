import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AccuracyDashboardComponent } from './accuracy-dashboard.component';

describe('AccuracyDashboardComponent', () => {
  let component: AccuracyDashboardComponent;
  let fixture: ComponentFixture<AccuracyDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AccuracyDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccuracyDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
