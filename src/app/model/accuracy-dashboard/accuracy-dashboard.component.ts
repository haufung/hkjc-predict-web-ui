import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../shared/services/dashboard/dashboard.service';

@Component({
  selector: 'app-accuracy-dashboard',
  templateUrl: './accuracy-dashboard.component.html',
  styleUrls: ['./accuracy-dashboard.component.scss'],
  providers: [DashboardService]
})
export class AccuracyDashboardComponent implements OnInit {

  loading = false;
  thresholdPerformance: any[];
  modelVersionDropdown: any = [];
  modelThresholdList: any[];
  modelSelectedThreshold: any;


  constructor(private dashboardService: DashboardService) { }

  async getThisModelThresholdEval(versionString) {
    await this.dashboardService.getModelThresholdEval({ version: versionString }).subscribe((res: any) => {
      if (res && res.code === 200) {
        this.thresholdPerformance.push.apply(this.thresholdPerformance, res.data)
        this.loading = false
      }
    });
  }

  customizeTooltip(arg: any) {
    var items = arg.valueText.split("\n"),
      color = arg.point.getColor();
    items.forEach(function (item, index) {
      if (item.indexOf(arg.seriesName) === 0) {
        var element = document.createElement("span");

        element.textContent = item;
        if (arg.seriesName == 'Accuracy') {
          element.style.color = "#00ABFF"
        } else {
          element.style.color = color;
        }
        element.className = "active";

        items[index] = element.outerHTML;
      }
    });
    items.push("Average Win Odds :" + arg.point.data.avg_win_odds)
    var element = document.createElement("span");
    if (arg.point.data.expected_profit > 1) {
      element.style.color = "#F3FF3C";
      element.textContent = "Expected Profit : +" + Math.round((arg.point.data.expected_profit - 1) * 100) + "%";
    } else {
      element.style.color = "#FF7676";
      element.textContent = "Expected Profit :" + Math.round((arg.point.data.expected_profit - 1) * 100) + "%";
    }
    element.className = "active";
    items.push(element.outerHTML);
    return { text: items.join("\n") };
  }

  ngOnInit(): void {
    this.loading = true
    this.thresholdPerformance = [];
    let modelVersion = JSON.parse(localStorage.getItem('modelVersion'));
    this.modelThresholdList = JSON.parse(localStorage.getItem('modelThreshold'));
    for (let version of modelVersion) {
      this.modelVersionDropdown.push(version.version)
    }
    this.loading = false
  }

  onModelVersionClick(event) {
    this.loading = true
    this.thresholdPerformance = [];
    this.getThisModelThresholdEval(event.itemData);
    if (this.modelThresholdList.find(x => x.version === event.itemData)) {
      this.modelSelectedThreshold = this.modelThresholdList.find(x => x.version === event.itemData).threshold;
    } else {
      this.modelSelectedThreshold = undefined
    }
  }

  changeToPercentage = (arg: any) => {
    return Math.round(arg.valueText * 100) + "%";
  }

  customizePoint = (arg: any) => {
    if (arg.data.expected_profit > 1 && arg.seriesName == 'Accuracy') {
      return { color: "#F3FF3C" , size: 7 };
    }
  }
}

