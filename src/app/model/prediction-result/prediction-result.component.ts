import { Component, OnInit } from '@angular/core';
import { PredictService } from '../../shared/services/predict/predict.service';
import { SocketService } from '../../shared/services/websocket/chat.service';
import * as moment from 'moment';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-prediction-result',
  templateUrl: './prediction-result.component.html',
  styleUrls: ['./prediction-result.component.scss', '../../shared/styles/main.scss'],
  providers: [PredictService, SocketService]
})
export class PredictionResultComponent implements OnInit {
  raceLocation: any;
  raceClass: any;
  raceDistance: any;
  raceGoing: any;
  raceCourse: any;
  raceName: any;
  raceIdDropdown: any = [];
  pastRaceIdDropdown: any = [];
  selectedRaceId: any;
  recordList: any = [];
  cols: any = [];
  headerCurrent: any = [
    { label: "馬匹", value: "chinese_name" },
    { label: "烙號", value: "horse_id" },
    { label: "騎師", value: "jockey_name" },
    { label: "檔位", value: "draw" },
    { label: "實際負磅", value: "actual_weight" },
    { label: "獨贏賠率", value: "win_odds" },
    { label: "分數", value: "confidence" }];
  headerPast: any = [
    { label: "結果排名", value: "place" },
    { label: "馬匹", value: "chinese_name" },
    { label: "烙號", value: "horse_id" },
    { label: "騎師", value: "jockey_name" },
    { label: "檔位", value: "draw" },
    { label: "實際負磅", value: "actual_weight" },
    { label: "獨贏賠率", value: "win_odds" },
    { label: "分數", value: "confidence" }];
  loading = false;
  loadingDropdown = false;

  messages: string[] = [];
  msgControl = new FormControl('');
  destroyed$ = new Subject();

  modelVersionDropdown: any = [];
  selectedVersion: any;
  disableSelectRace = true;


  constructor(
    private predictService: PredictService,
    private chatService: SocketService) { }

  async getThisCurrentRecordsData(id, version) {
    await this.predictService.searchCurrentRecordsByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
      if (res && res.code === 200) {
        res.data.forEach(element => {
          this.recordList.push(element)
        });
      }
    });
  }

  async getThisPastRecordsData(id, version) {
    await this.predictService.searchPastRecordsByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
      if (res && res.code === 200) {
        res.data.forEach(element => {
          this.recordList.push(element)
        });
      }
    });
  }

  mapRaceInfoToHeader(res) {
    this.raceLocation = res.data[0].location
    this.raceClass = res.data[0].class
    this.raceDistance = res.data[0].distance
    this.raceGoing = res.data[0].going
    this.raceCourse = res.data[0].course
    this.raceName = res.data[0].race_name_english
  }

  async getThisCurrentRaceData(id, version) {
    await this.predictService.searchCurrentRaceByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
      if (res && res.code === 200 && res.data.length > 0) {
        this.mapRaceInfoToHeader(res)
      }
      this.loading = false;
    });
  }

  async getThisPastRaceData(id, version) {
    await this.predictService.searchPastRaceByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
      if (res && res.code === 200 && res.data.length > 0) {
        this.mapRaceInfoToHeader(res)
      }
      this.loading = false;
    });
  }

  resetRaceData() {
    this.raceLocation = ''
    this.raceClass = ''
    this.raceDistance = ''
    this.raceGoing = ''
    this.raceCourse = ''
    this.raceName = ''
  }

  onRowUnselect(event) {
    this.recordList = []
    this.cols = []
  }
  onSelectRaceId(event) {
    this.loading = true;
    this.selectedRaceId = event
    this.recordList = []
    this.resetRaceData()
    this.cols = this.headerCurrent
    this.getThisCurrentRecordsData(this.selectedRaceId, this.selectedVersion)
    this.getThisCurrentRaceData(this.selectedRaceId, this.selectedVersion)
  }

  onSelectPastRaceId(event) {
    this.loading = true;
    this.selectedRaceId = event
    this.recordList = []
    this.cols = this.headerPast
    this.resetRaceData()
    this.getThisPastRecordsData(this.selectedRaceId, this.selectedVersion)
    this.getThisPastRaceData(this.selectedRaceId, this.selectedVersion)
  }

  clearAllRaceIdDropdown() {
    this.pastRaceIdDropdown = []
    this.raceIdDropdown = []
    this.selectedRaceId = undefined
  }

  onSelectModelVersion(event) {
    this.selectedVersion = event
    this.recordList = []
    this.resetRaceData()
    this.disableSelectRace = false
    this.loadingDropdown = true;
    this.clearAllRaceIdDropdown()
    this.getAllCurrentRaceId(this.selectedVersion)
    this.getAllPastRaceId(this.selectedVersion)
  }

  getRowClass(row) {
    if (row.confidence > 0.87) {
      return 'highlight-row'
    }
  }

  getDataClass(column, row) {
    if (column == 'confidence' && row.confidence > 0.87) {
      return 'highlight-word'
    }
  }

  mapRaceLabelFromRaceId(item) {
    var idAndRaceNum = item.race_id.split("_")
    var raceNum = idAndRaceNum.pop()
    var dateId = idAndRaceNum.pop()
    var date = moment(dateId, 'YYYYMMDD')
    var dateLabel = date.format('YYYY年M月DD日');
    return [raceNum, dateLabel]
  }

  async getAllPastRaceId(version) {
    await this.predictService.listAllPastRaceId({ version: version }).subscribe((res: any) => {
      if (res && res.code === 200) {
        res.data.forEach(item => {
          var [raceNum, dateLabel] = this.mapRaceLabelFromRaceId(item)
          this.pastRaceIdDropdown.push({ label: dateLabel + `第${raceNum}場`, value: item.race_id })
        });
      }
      this.loadingDropdown = false;
    });
  }

  async getAllCurrentRaceId(version) {
    await this.predictService.listAllCurrentRaceId({ version: version }).subscribe((res: any) => {
      if (res && res.code === 200) {
        res.data.forEach(item => {
          var [raceNum, dateLabel] = this.mapRaceLabelFromRaceId(item)
          this.raceIdDropdown.push({ label: dateLabel + `第${raceNum}場`, value: item.race_id })
        });
      }
    });
  }

  ngOnInit(): void {
    this.modelVersionDropdown = JSON.parse(localStorage.getItem('modelVersion'));


    const chatSub$ = this.chatService.connect().pipe(
      takeUntil(this.destroyed$),
    );

    chatSub$.subscribe(message => {
      console.log(message)
      this.messages.push(message)
    });
  }

  sendMessage(): void {
    this.chatService.send(this.msgControl.value);
    this.msgControl.setValue('');
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
