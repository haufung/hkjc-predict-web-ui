import { Component, OnInit, Input, ViewChild, HostListener, TemplateRef } from '@angular/core';
import * as moment from 'moment';
import { PredictService } from '../../shared/services/predict/predict.service';
import { ScrapperService } from '../../shared/services/scrapper/scrapper.service';
import { DashboardService } from '../../shared/services/dashboard/dashboard.service';
import { DxChartComponent } from "devextreme-angular";
import { NbDialogService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-dashboard',
  templateUrl: './home-dashboard.component.html',
  styleUrls: ['./home-dashboard.component.scss', '../../shared/styles/main.scss'],
  providers: [PredictService, ScrapperService, DashboardService]
})
export class HomeDashboardComponent implements OnInit {
  @ViewChild("targetChart", { static: false }) dxChart: DxChartComponent
  @Input() nextRaceDate: string;

  checkWindowSizeAndLoad(isInit) {
    this.deviceWidth = window.innerWidth;
    this.deviceHeight = window.innerHeight;
    if (this.deviceWidth < 795) {
      if (!this.isSmallScreen || isInit == true) {
        this.isSmallScreen = true
        this.changeToAccordion = true
        this.smallScreenLoad()
      }
    } else {
      if (this.isSmallScreen || isInit == true) {
        this.isSmallScreen = false
        this.changeToAccordion = false
        this.nonSmallScreenLoad()
      }
    }
  }
  @HostListener('window: resize', ['$event']) onResize(event: any) {
    this.checkWindowSizeAndLoad(false)
  }
  loading = false;
  latestModelVersion: any;
  firstRaceId = { label: "", value: "" };
  currentRaceData: any = [];
  modelPastPredict: any = [];
  allEloOfCurrentRace: any = [];
  raceIdDropdown: any = [];
  recordList: any = [];
  latestVersionThreshold: any;
  cols: any = [];
  raceLocation: any;
  raceClass: any;
  raceDistance: any;
  raceGoing: any;
  raceCourse: any;
  raceName: any;
  thresholdText: any;
  displayPastMonthWin = "12";
  averageHorseElo: any;
  averageJockeyElo: any;
  averageTrainerElo: any;
  horseEloSelected: boolean;
  jockeyEloSelected: boolean;
  trainerEloSelected: boolean;
  defaultEloBarColor = "#e7d19a";
  darkEloBarColor = "#413C2B";
  horseEloBarColor: any;
  jockeyEloBarColor: any;
  trainerEloBarColor: any;
  deviceHeight: any;
  deviceWidth: any;
  isSmallScreen = false;
  changeToAccordion = false;
  horseEloDefaultColor = "#ff7c7c";
  jockeyEloDefaultColor = "#AAAAFA";
  trainerEloDefaultColor = "#00BFFF";
  selectedRaceId: any;
  selectedHorseNumber: any;
  raceAccordionForMobile: any = [];
  currentRaceDate: any;

  headerCurrent: any = [
    { label: "馬號", value: "horse_number" },
    { label: "馬匹", value: "chinese_name" },
    { label: "騎師", value: "jockey_name" },
    { label: "檔位", value: "draw" },
    { label: "實際負磅", value: "actual_weight" },
    { label: "獨贏賠率", value: "win_odds" },
    { label: "分數", value: "confidence" }];

  constructor(private predictService: PredictService,
    private scrapperService: ScrapperService,
    private dashboardService: DashboardService,
    private dialogService: NbDialogService,
    private router: Router) { }

  async getThisCurrentRecordsData(id, version) {
    return new Promise<void>((resolve, reject) => {
      this.predictService.searchCurrentRecordsByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
        let recordListData = []
        let currentRaceListData = []
        if (res && res.code === 200) {
          res.data.forEach(element => {
            element.confidence = parseFloat(element.confidence).toFixed(3);
            element.win_odds = +element.win_odds || null;
            recordListData.push(element)
            element.horseNameAndNum = `${element.horse_number}-${element.chinese_name}`
            element.confidence = parseFloat(element.confidence)
            element.latestVersionThreshold = parseFloat(this.latestVersionThreshold)
            currentRaceListData.push(element)
          });
        }
        this.recordList = recordListData
        this.currentRaceData = currentRaceListData
        resolve()
      });
    });
  }

  async getThisCurrentRaceData(id, version) {
    return new Promise<void>((resolve, reject) => {
      this.predictService.searchCurrentRaceByRaceId({ raceId: id, version: version }).subscribe((res: any) => {
        if (res && res.code === 200 && res.data.length > 0) {
          this.mapRaceInfoToHeader(res)
        }
        resolve()
      });
    });
  }

  async getModelPastPredict(version, threshold, numberOfMonths) {
    return new Promise<void>((resolve, reject) => {
      this.dashboardService.getPastPredictWithinMonthsWithThreshold({ version: version, threshold: threshold, numberOfMonths: numberOfMonths }).subscribe((res: any) => {
        if (res && res.code === 200 && res.data.length > 0) {
          res.data.forEach(item => {
            var [raceNum, dateLabel] = this.mapRaceLabelFromRaceId(item)
            var [raceNum, dateLabel] = this.mapShortRaceLabelFromRaceId(item)
            item.race_id = dateLabel + `第${raceNum}場`
            item.short_race_id = dateLabel + `(${raceNum})`
            item.isWin = item.place == '1' || item.place == '1 DH' ? true : false
            item.firstRunner = item.place == '2' || item.place == '2 DH' ? true : false
            item.secondRunner = item.place == '3' || item.place == '3 DH' ? true : false
            item.longPlace = `第${item.place}名`
            item.round = parseFloat(item.round).toFixed(3);
          });
          this.modelPastPredict = res.data
        }
        resolve()
      });
    });
  }

  async getAllEloByCurrentRaceId(raceId, version) {
    return new Promise<void>((resolve, reject) => {
      this.dashboardService.getAllEloByCurrentRaceId({ version: version, race_id: raceId }).subscribe((res: any) => {
        if (res && res.code === 200 && res.data.length > 0) {
          res.data.forEach(item => {
            item.horseNameAndNum = `${item.horse_number}-${item.horse_name}`
            // To be used in CustomizeEloToolTip
            item.averageHorseElo = this.averageHorseElo
            item.averageJockeyElo = this.averageJockeyElo
            item.averageTrainerElo = this.averageTrainerElo
            item.horseEloDefaultColor = this.horseEloDefaultColor
            item.jockeyEloDefaultColor = this.jockeyEloDefaultColor
            item.trainerEloDefaultColor = this.trainerEloDefaultColor
            item.defaultEloBarColor = this.defaultEloBarColor
            item.darkEloBarColor = this.darkEloBarColor
          })
          this.allEloOfCurrentRace = res.data
        }
        resolve()
      });
    });
  }

  mapRaceInfoToHeader(res) {
    this.raceLocation = res.data[0].location
    this.raceClass = res.data[0].class
    this.raceDistance = res.data[0].distance
    this.raceGoing = res.data[0].going
    this.raceCourse = res.data[0].course
    this.raceName = res.data[0].race_name_english
  }

  async onRaceIdClick(event) {
    this.loading = true
    this.recordList = []
    this.currentRaceData = []
    this.selectedRaceId = event.itemData.value
    await this.getThisCurrentRecordsData(this.selectedRaceId, this.latestModelVersion)
    await this.getThisCurrentRaceData(this.selectedRaceId, this.latestModelVersion)
    await this.getAllEloByCurrentRaceId(this.selectedRaceId, this.latestModelVersion)
    this.loading = false
  }

  customizePoint = (arg: any) => {
    if (arg.value > this.latestVersionThreshold) {
      return { color: "#ff7c7c", hoverStyle: { color: "#ff7c7c" } };
    }
  }

  customizeLabel = (arg: any) => {
    if (arg.value > this.latestVersionThreshold) {
      return {
        visible: true,
        backgroundColor: "#ff7c7c",
        customizeText: function (e: any) {
          return e.valueText;
        }
      };
    }
  }

  customizeText = (arg: any) => {
    return arg.valueText;
  }

  customizeTooltip(arg: any) {
    var items = arg.valueText.split("\n");
    items.forEach(function (item, index) {
      if (item.indexOf(arg.seriesName) === 0) {
        var element = document.createElement("span");

        element.textContent = item;
        if (arg.point.data.confidence > arg.point.data.latestVersionThreshold) {
          element.style.color = "#ff7c7c"
        } else {
          element.style.color = "#e7d19a"
        }
        element.className = "active";

        items[index] = element.outerHTML;
      }
    });
    items.push("馬號 : " + arg.point.data.horse_number)
    items.push("騎師 : " + arg.point.data.jockey_name)
    items.push("檔位 : " + arg.point.data.draw)
    items.push("實際負磅 : " + arg.point.data.actual_weight)
    items.push("獨贏賠率 : " + arg.point.data.win_odds)
    return { text: items.join("\n") };
  }

  mapRaceLabelFromRaceId(item) {
    var idAndRaceNum = item.race_id.split("_")
    var raceNum = idAndRaceNum.pop()
    var dateId = idAndRaceNum.pop()
    var date = moment(dateId, 'YYYYMMDD')
    var dateLabel = date.format('YYYY年M月DD日');
    return [raceNum, dateLabel]
  }

  mapShortRaceLabelFromRaceId(item) {
    var idAndRaceNum = item.race_id.split("_")
    var raceNum = idAndRaceNum.pop()
    var dateId = idAndRaceNum.pop()
    var date = moment(dateId, 'YYYYMMDD')
    var dateLabel = date.format('DD/M/YYYY');
    return [raceNum, dateLabel]
  }

  async getAllCurrentRaceId(version) {
    return new Promise<void>((resolve, reject) => {
      this.predictService.listAllCurrentRaceId({ version: version }).subscribe((res: any) => {
        let raceIdDropdownList = []
        if (res && res.code === 200) {
          res.data.forEach(item => {
            var [raceNum, dateLabel] = this.mapRaceLabelFromRaceId(item)
            raceIdDropdownList.push({ label: dateLabel + `第${raceNum}場`, value: item.race_id })
          });
        }
        this.raceIdDropdown = raceIdDropdownList
        resolve()
      });
    })
  }

  getAllCurrentRaceIdDetailsFromSessionStorage() {
    var currentRaceIdDetails = JSON.parse(sessionStorage.getItem('currentRaceIdDetails'))
    var [_, dateLabel] = this.mapRaceLabelFromRaceId(currentRaceIdDetails[0])
    this.currentRaceDate = dateLabel
    this.raceAccordionForMobile = currentRaceIdDetails
  }

  async smallScreenLoad() {
    await this.getAllCurrentRaceIdDetailsFromSessionStorage()
  }

  async nonSmallScreenLoad() {
    await this.getAllCurrentRaceId(this.latestModelVersion)
    this.firstRaceId = this.raceIdDropdown[0]
    this.selectedRaceId = this.firstRaceId
    await this.getThisCurrentRecordsData(this.firstRaceId.value, this.latestModelVersion)
    this.cols = this.headerCurrent
    await this.getThisCurrentRaceData(this.firstRaceId.value, this.latestModelVersion)
    await this.getAllEloByCurrentRaceId(this.firstRaceId.value, this.latestModelVersion)
  }

  async ngOnInit() {
    this.loading = true
    this.nextRaceDate = localStorage.getItem('nextRaceDate')
    this.latestModelVersion = JSON.parse(localStorage.getItem('latestModelVersion'))[0].version;
    this.averageHorseElo = localStorage.getItem('averageHorseElo')
    this.averageJockeyElo = localStorage.getItem('averageJockeyElo')
    this.averageTrainerElo = localStorage.getItem('averageTrainerElo')
    JSON.parse(localStorage.getItem('modelThreshold')).map(model => {
      if (model.version == this.latestModelVersion) {
        this.latestVersionThreshold = model.threshold
      }
    });
    this.thresholdText = `建議門檻-${this.latestVersionThreshold}`
    this.checkWindowSizeAndLoad(true)
    await this.getModelPastPredict(this.latestModelVersion, "0.5", this.displayPastMonthWin)
    this.loading = false
  }

  getDataClass(column, row) {
    if (column == 'confidence' && (row.confidence > this.latestVersionThreshold)) {
      return 'highlight-word'
    }
  }

  resetEloSelect() {
    this.horseEloSelected = false
    this.jockeyEloSelected = false
    this.trainerEloSelected = false
  }

  resetEloBarColor() {
    this.horseEloBarColor = this.darkEloBarColor
    this.jockeyEloBarColor = this.darkEloBarColor
    this.trainerEloBarColor = this.darkEloBarColor
  }

  changeEloTab(event) {
    this.resetEloSelect()
    this.resetEloBarColor()
    if (event.tabTitle.includes('馬匹')) {
      this.horseEloSelected = true
      this.horseEloBarColor = this.defaultEloBarColor
    } else if (event.tabTitle.includes('騎師')) {
      this.jockeyEloSelected = true
      this.jockeyEloBarColor = this.defaultEloBarColor
    } else if (event.tabTitle.includes('練馬師')) {
      this.trainerEloSelected = true
      this.trainerEloBarColor = this.defaultEloBarColor
    }
    this.dxChart.instance.refresh();
  }

  onEloPointClick(e: any) {
    var point = e.target;
    point.showTooltip();
  }

  onEloPointHoverChanged(e) {
    let point = e.target;
    if (!point.isHovered()) {
      point.hideTooltip();
    }
  }

  onClickRaceId(selectedRaceId) {
    this.router.navigate(['mobile/race/' + selectedRaceId]);
  }

  customizeEloPoint = (arg: any) => {
    if (this.horseEloSelected && arg.seriesName == '馬匹積分' && arg.value > this.averageHorseElo) {
      return { color: this.horseEloDefaultColor, hoverStyle: { color: this.horseEloDefaultColor } };
    } else if (this.jockeyEloSelected && arg.seriesName == '騎師積分' && arg.value > this.averageJockeyElo) {
      return { color: this.jockeyEloDefaultColor, hoverStyle: { color: this.jockeyEloDefaultColor } };
    } else if (this.trainerEloSelected && arg.seriesName == '練馬師積分' && arg.value > this.averageTrainerElo) {
      return { color: this.trainerEloDefaultColor, hoverStyle: { color: this.trainerEloDefaultColor } };
    }
  }

  customizeEloLabel = (arg: any) => {
    if (this.horseEloSelected && arg.seriesName == '馬匹積分' && arg.value > this.averageHorseElo) {
      return {
        visible: true,
        backgroundColor: this.horseEloDefaultColor,
        customizeText: function (e: any) {
          return e.valueText;
        }
      };
    } else if (this.jockeyEloSelected && arg.seriesName == '騎師積分' && arg.value > this.averageJockeyElo) {
      return {
        visible: true,
        backgroundColor: this.jockeyEloDefaultColor,
        customizeText: function (e: any) {
          return e.valueText;
        }
      };
    } else if (this.trainerEloSelected && arg.seriesName == '練馬師積分' && arg.value > this.averageTrainerElo) {
      return {
        visible: true,
        backgroundColor: this.trainerEloDefaultColor,
        customizeText: function (e: any) {
          return e.valueText;
        }
      };
    }
  }

  onClickRecords(dialog: TemplateRef<any>, horseNumber) {
    this.selectedHorseNumber = horseNumber
    this.dialogService.open(dialog);
  }

  customizeEloTooltip(arg: any) {
    var items = arg.valueText.split("\n");
    var addedItems = []
    items.forEach(function (item, index) {
      var element = document.createElement("span");
      element.textContent = item;
      if (item.includes('馬匹積分')) {
        var elementHorse = document.createElement("span");
        elementHorse.textContent = arg.point.data.horse_name;
        elementHorse.className = "active";
        if (arg.point.data.horse_elo_before > arg.point.data.averageHorseElo) {
          elementHorse.style.color = arg.point.data.horseEloDefaultColor
          element.style.color = arg.point.data.horseEloDefaultColor
        } else {
          elementHorse.style.color = arg.point.data.defaultEloBarColor
          element.style.color = arg.point.data.defaultEloBarColor
        }
        addedItems.push(elementHorse.outerHTML);
      }
      if (item.includes('騎師積分')) {
        var elementJockey = document.createElement("span");
        elementJockey.textContent = arg.point.data.jockey_name;
        elementJockey.className = "active";
        if (arg.point.data.jockey_elo_before > arg.point.data.averageJockeyElo) {
          elementJockey.style.color = arg.point.data.jockeyEloDefaultColor
          element.style.color = arg.point.data.jockeyEloDefaultColor
        } else {
          elementJockey.style.color = arg.point.data.defaultEloBarColor
          element.style.color = arg.point.data.defaultEloBarColor
        }
        addedItems.push(elementJockey.outerHTML);
      }

      if (item.includes('練馬師積分')) {
        var elementTrainer = document.createElement("span");
        elementTrainer.textContent = arg.point.data.trainer_name;
        elementTrainer.className = "active";
        if (arg.point.data.trainer_elo_before > arg.point.data.averageTrainerElo) {
          elementTrainer.style.color = arg.point.data.trainerEloDefaultColor
          element.style.color = arg.point.data.trainerEloDefaultColor
        } else {
          elementTrainer.style.color = arg.point.data.defaultEloBarColzor
          element.style.color = arg.point.data.defaultEloBarColor
        }
        addedItems.push(elementTrainer.outerHTML);
      }
      element.className = "active";
      addedItems.push(element.outerHTML);
    });
    return { text: addedItems.join("\n") };
  }

}
