import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../shared/services/dashboard/dashboard.service';
import * as moment from 'moment';

@Component({
  selector: 'app-performance-dashboard',
  templateUrl: './performance-dashboard.component.html',
  styleUrls: ['./performance-dashboard.component.scss', '../../shared/styles/main.scss'],
  providers: [DashboardService]
})
export class PerformanceDashboardComponent implements OnInit {
  modelPerformanceWithoutThreshold: any[]
  modelPerformanceWithThreshold: any[]
  loading = false;

  constructor(private dashboardService: DashboardService) { }

  async getThisModelPerformance(versionString) {
    return new Promise<void>((resolve, reject) => {
      this.dashboardService.getModelPerformance({ version: versionString }).subscribe((res: any) => {
        if (res && res.code === 200) {
          this.modelPerformanceWithoutThreshold.push.apply(this.modelPerformanceWithoutThreshold, res.data)
          resolve()
        }
      });
    });
  }

  async getThisModelPerformanceThreshold(versionString) {
    return new Promise<void>((resolve, reject) => {
      this.dashboardService.getModelPerformanceThreshold({ version: versionString }).subscribe((res: any) => {
        if (res && res.code === 200) {
          this.modelPerformanceWithThreshold.push.apply(this.modelPerformanceWithThreshold, res.data)
          resolve()
        }
      });
    });
  }

  async ngOnInit() {
    this.loading = true
    this.modelPerformanceWithoutThreshold = [];
    this.modelPerformanceWithThreshold = [];
    let modelVersion = JSON.parse(localStorage.getItem('modelVersion'));
    for (let version of modelVersion) {
      await this.getThisModelPerformance(version.version);
      await this.getThisModelPerformanceThreshold(version.version);
    }
    this.loading = false
  }

  addDollarSign = (arg: any) => {
    return "$" + arg.valueText;
  }
  parseToRace = (arg: any) => {
    var idAndRaceNum = arg.valueText.split("_")
    var raceNum = idAndRaceNum.pop()
    var dateId = idAndRaceNum.pop()
    var date = moment(dateId, 'YYYYMMDD')
    var dateLabel = date.format('YYYY年M月DD日');
    return dateLabel + ` 第${raceNum}場`
  }

}
