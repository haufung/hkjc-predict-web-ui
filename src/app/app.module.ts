import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//PrimeNg
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { HttpClientModule } from '@angular/common/http';
import { PanelModule } from 'primeng/panel';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { CardModule } from 'primeng/card';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import {TranslateModule} from '@ngx-translate/core';

// Nebular
import { NbProgressBarModule, NbButtonGroupModule, NbDialogModule, NbThemeModule, NbSidebarModule, NbLayoutModule, NbButtonModule, NbSelectModule, NbCardModule, NbMenuModule, NbSpinnerModule, NbListModule, NbTabsetModule, NbIconModule, NbPopoverModule, NbAccordionModule,} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

// DevExtreme
import { DxChartModule, DxSelectBoxModule } from 'devextreme-angular';

import { PerformanceDashboardComponent } from './model/performance-dashboard/performance-dashboard.component';
import { PredictionResultComponent } from './model/prediction-result/prediction-result.component';
import { AccuracyDashboardComponent } from './model/accuracy-dashboard/accuracy-dashboard.component';

import { HeaderModule } from './shared/components/header/header.module';
import { HomeDashboardComponent } from './model/home-dashboard/home-dashboard.component';
import { ModelPastPredictComponent } from './mobile/model-past-predict/model-past-predict.component';
import { HomeDashboardRaceDetailsComponent } from './mobile/home-dashboard-race-details/home-dashboard-race-details.component';
import { RecordsDetailsComponent } from './mobile/records-details/records-details.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    PredictionResultComponent,
    PerformanceDashboardComponent,
    AccuracyDashboardComponent,
    HomeDashboardComponent,
    ModelPastPredictComponent,
    HomeDashboardRaceDetailsComponent,
    RecordsDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TableModule,
    ButtonModule,
    ToastModule,
    HttpClientModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbSidebarModule.forRoot(), // NbSidebarModule.forRoot(), //if this is your app.module
    NbButtonModule,
    NbSelectModule,
    NbCardModule,
    PanelModule,
    MessageModule,
    MessagesModule,
    BrowserAnimationsModule,
    NbCardModule,
    DxChartModule,
    NbMenuModule.forRoot(),
    NbEvaIconsModule,
    NbSpinnerModule,
    CardModule,
    ProgressSpinnerModule,
    HeaderModule,
    DxSelectBoxModule,
    NbListModule,
    NbTabsetModule,
    NbIconModule,
    NbPopoverModule,
    NbAccordionModule,
    NbDialogModule.forRoot(),
    NbButtonGroupModule,
    TranslateModule.forRoot({
      defaultLanguage: 'HK'
  }),
  NbProgressBarModule,
  ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
