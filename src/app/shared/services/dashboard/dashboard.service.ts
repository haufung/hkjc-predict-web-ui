import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import qs from 'querystring';
import { environment as env } from '../../../../environments/environment';

@Injectable()
export class DashboardService {
  constructor(private http: HttpClient) { }
  getModelPerformance(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/dashboard/modelPerformance?${params}`;
    return this.http.get(url);
  }
  getModelPerformanceThreshold(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/dashboard/modelPerformanceThreshold?${params}`;
    return this.http.get(url);
  }

  getModelThresholdEval(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/dashboard/modelThresholdEval?${params}`;
    return this.http.get(url);
  }

  getPastPredictWithinMonthsWithThreshold(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/dashboard/getPastPredictWithinMonthsWithThreshold?${params}`;
    return this.http.get(url);
  }

  getAllEloByCurrentRaceId(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/dashboard/getAllEloByCurrentRaceId?${params}`;
    return this.http.get(url);
  }

  getAllEloAverage() {
    let url = `${env.baseUrl}/api/dashboard/getAllEloAverage`;
    return this.http.get(url);
  }
}