import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private http: HttpClient) { }
  getModelVersion() {
    let url = `${env.baseUrl}/api/config/modelVersion`;
    return this.http.get(url);
  }

  getModelThreshold() {
    let url = `${env.baseUrl}/api/config/modelThreshold`;
    return this.http.get(url);
  }

  getLatestModelVersion() {
    let url = `${env.baseUrl}/api/config/latestModelVersion`;
    return this.http.get(url);
  }

  getTranslateMapping() {
    let url = `${env.baseUrl}/api/config/translateMapping`;
    return this.http.get(url);
  }
}
