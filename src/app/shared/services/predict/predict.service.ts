import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import qs from 'querystring';
import { environment as env } from '../../../../environments/environment';

@Injectable()
export class PredictService {
  constructor(private http: HttpClient) { }
  listAllCurrentRaceId(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/predict/listAllCurrentRaceId?${params}`;
    return this.http.get(url);
  }
  listAllPastRaceId(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/predict/listAllPastRaceId?${params}`;
    return this.http.get(url);
  }
  searchCurrentRaceByRaceId(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/predict/getCurrentRaceByRaceId?${params}`;
    return this.http.get(url);
  }
  searchCurrentRecordsByRaceId(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/predict/getCurrentRecordsByRaceId?${params}`;
    return this.http.get(url);
  }
  searchPastRaceByRaceId(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/predict/getPastRaceByRaceId?${params}`;
    return this.http.get(url);
  }
  searchPastRecordsByRaceId(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/predict/getPastRecordsByRaceId?${params}`;
    return this.http.get(url);
  }
  getAllCurrentRaceIdDetails() {
    let url = `${env.baseUrl}/api/predict/getAllCurrentRaceIdDetails`;
    return this.http.get(url);
  }
  getCurrentRecordsBasicInfoByRaceIdAndHorseNum(param) {
    const params = qs.stringify(param);
    let url = `${env.baseUrl}/api/predict/getCurrentRecordsBasicInfoByRaceIdAndHorseNum?${params}`;
    return this.http.get(url);
  }
}