"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.PredictService = void 0;
var core_1 = require("@angular/core");
var querystring_1 = require("querystring");
var PredictService = /** @class */ (function () {
    function PredictService(http) {
        this.http = http;
    }
    PredictService.prototype.listAllCurrentRaceId = function () {
        var url = "http://localhost:3000/predict/listPredict";
        return this.http.get(url);
    };
    PredictService.prototype.searchByRaceId = function (param) {
        var params = querystring_1["default"].stringify(param);
        var url = "http://localhost:3000/predict/getCurrentRecordsByRaceId?" + params;
        return this.http.get(url);
    };
    PredictService.prototype.listAllPastRaceId = function () {
        var url = "http://localhost:3000/predict/listAllPastRaceId";
        return this.http.get(url);
    };
    PredictService.prototype.searchPastPredictByRaceId = function (param) {
        var params = querystring_1["default"].stringify(param);
        var url = "http://localhost:3000/predict/getPastRecordsByRaceId?" + params;
        return this.http.get(url);
    };
    PredictService = __decorate([
        core_1.Injectable()
    ], PredictService);
    return PredictService;
}());
exports.PredictService = PredictService;
