import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScrapperService {

  constructor(private http: HttpClient) { }
  getNextRaceDate() {
    let url = `${env.baseUrl}/api/scrapper/queryNextRaceDate`;
    return this.http.get(url);
  }
}
