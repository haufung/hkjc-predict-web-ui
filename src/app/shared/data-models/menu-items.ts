import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [{
    title: '主頁',
    icon: 'home-outline',
    link: 'start'
},
// {
//     title: 'Model Performance',
//     icon: 'activity-outline',
//     link: 'performance'
// },
// {
//     title: 'Threshold Accuracy',
//     icon: 'eye-outline',
//     link: 'accuracy'
// },
{
    title: '過往賽事',
    icon: 'archive-outline',
    link: 'result'
}
];

export const SMALL_SCREEN_MENU_ITEMS: NbMenuItem[] = [{
    title: '主頁',
    icon: 'home-outline',
    link: 'start'
},{
    title: 'AI往績',
    icon: 'activity-outline',
    link: 'mobile/past'
},
{
    title: '過往賽事',
    icon: 'archive-outline',
    link: 'result'
}
];