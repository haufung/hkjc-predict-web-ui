import { NgModule } from '@angular/core';
import { HeaderComponent } from './header.component';
import { NbIconModule, NbSelectModule, NbUserModule, NbButtonModule, NbActionsModule, NbTooltipModule } from '@nebular/theme';
import { BrowserModule } from '@angular/platform-browser';
import {ClipboardModule} from '@angular/cdk/clipboard';

@NgModule({
  imports: [
    NbIconModule,
    NbSelectModule,
    BrowserModule,
    NbUserModule,
    NbButtonModule,
    NbActionsModule,
    ClipboardModule,
    NbTooltipModule,
  ],
  declarations: [
    HeaderComponent
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule { }
