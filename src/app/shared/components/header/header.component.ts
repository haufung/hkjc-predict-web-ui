import { Component, OnInit, HostListener} from '@angular/core';
import { NbThemeService, NbSidebarService } from '@nebular/theme';
import { LayoutService } from '../../../@core/utils';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss', '../../styles/main.scss'],
  providers: [LayoutService]
})
export class HeaderComponent implements OnInit {
  showHeaderArrow = false;
  companyEmail = 'contact@megaintelligence.net';
  _: any;
  isSmallScreen = false
  constructor(
    private layoutService: LayoutService,
    private sidebarService: NbSidebarService,
    private router: Router,) { 
    }

    checkWindowSize() {
    if (window.innerWidth < 795) {
      this.isSmallScreen = true
    } else {
      this.isSmallScreen = false
    }
  }
  
  @HostListener('window: resize', ['$event']) onResize(event: any) {
    this.checkWindowSize()
  }

  ngOnInit(): void {
    this.checkWindowSize()
    this._ = this.router.events.pipe(filter(event => event instanceof NavigationEnd)).subscribe(event => this.modifyHeader(event));
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  backArrowClick() {
    this.router.navigate(['start']);
  }

  modifyHeader(location) {
    if (location.urlAfterRedirects.includes("/mobile/race")) {
      this.showHeaderArrow = true;
    } else {
      this.showHeaderArrow = false;
    }
  }

  ngOnDestroy() {
    this._.unsubscribe();
  }
}
