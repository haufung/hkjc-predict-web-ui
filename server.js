const express = require('express');
const cors = require('cors');
const config = require('config');
const path = require('path');
// AWS.config.update({ region: 'ap-east-1' });
// const dynamodb = new AWS.DynamoDB();

let environment = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev';
console.log('node env:', process.env.NODE_ENV);

const port = process.env.PORT || 5000;

const request = require('request');

const app = express();
app.use(cors());
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        console.error(err);
        res.status(400).send({
            code: 400,
            message: 'Request body is not in proper JSON format.'
        })
    }
    next();
})
app.all('/api/*', (req, res) => {
    let backend = config.get(`${environment}.apiDomains.backend`);
    if (req.method === 'GET' || req.method === 'DELETE') {
        var options = {
            url: `${backend}${req.url.replace("api/", "")}`,
            method: req.method
        };
        request(options, (error, response) => {
            if (error) {
                console.error(error);
                res.status(500).send({
                    code: error.code,
                    message: error.message
                })
            } else if (response && response.body) {
                res.setHeader("Content-Type", response.headers['content-type']);
                let responseObj
                try {
                    responseObj = typeof response.body === 'object' ? response.body : JSON.parse(response.body);
                    res.status(response.statusCode).send({
                        code: responseObj.code,
                        data: responseObj.data,
                        message: responseObj.message
                    })
                } catch (err) {
                    console.log(err)
                    console.log(responseObj)
                }
            }
        })
    } else if (req.method === 'POST' || req.method === 'PUT') {
        var options = {
            url: `${backend}${req.url}`,
            headers: {
                "content-type": "application/json",

            },
            json: true,
            body: req.body,
            method: req.method
        };

        request(options, (error, response) => {
            if (error) {
                console.log('error:', error);
                res.status(response.statusCode).send({
                    code: response.statusCode,
                    message: error
                });
            } else if (response && response.body) {
                res.setHeader("Content-Type", response.headers['content-type']);
                let responseObj
                try {
                    responseObj = typeof response.body === 'object' ? response.body : JSON.parse(response.body);
                    res.status(response.statusCode).send({
                        code: responseObj.code,
                        data: responseObj.data,
                        message: responseObj.message
                    })
                } catch (err) {
                    console.log(err)
                    console.log(responseObj)
                }
            }
        })
    }
});

// function getApiKey() {
//     try {
//       if (!security) {
//         console.error("Missing username or password on security header")
//         return false
//       };

//       let token, password, username;

//       token = security.UsernameToken;
//       if (!token) {
//         console.error("Missing username or password on security header")
//         return false
//       };

//       username = token.Username;
//       password = token.Password;
//       if (!username || !password) {
//         console.error("Missing username or password on security header")
//         return false
//       };
//       password = password['$value'];

//       let params = {
//         TableName: 'she-crm-integration-service-users',
//         Key: {
//           'key': { S: username }
//         }
//       };

//       dynamodb.getItem(params, function (err, data) {
//         if (err) {
//           console.error(err)
//           callback(false);
//         } else {
//           if (!data || !data.Item) {
//             console.error("dynamodb : Invalid username or password on security header")
//             callback(false)
//           };

//           let password_hash = data.Item.password_hash.S;
//           let env_name = data.Item.env_name.S;

//           if (!password_hash || !env_name) {
//             console.error("dynamodb : Invalid username or password on security header")
//             callback(false)
//           };

//           let receivedPasswordHash = crypto.createHash('md5').update(password).digest("hex");
//           if (!(password_hash === receivedPasswordHash && env_name === process.env.ENV_NAME)) {
//             console.error("dynamodb : Invalid username or password on security header")
//           }
//           callback(password_hash === receivedPasswordHash && env_name === process.env.ENV_NAME);
//         }
//       });
//     } catch (err) {
//       console.error(err)
//       callback(false);
//     }
//   }

app.listen(port, () => console.log(`App started on port ${port}`));
